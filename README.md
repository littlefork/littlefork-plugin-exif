# littlefork-plugin-exif

THIS PLUGIN WAS DEPRECATED IN FAVOR OF THE [MEDIA PLUGIN](https://gitlab.com/littlefork/littlefork-plugin-media).

This is a plugin for [littlefork](https://github.com/tacticaltech/littlefork).
Extract the exif data of images.

## Requirements

- `graphicsmagick`.

## Installation

```
npm install --save littlefork-plugin-exif
```

## Usage

This plugin exports a single transformation plugin:

### `exif` transformation

```
$(npm bin)/littlefork -c cfg.json -p ddg,exif
```

This extracts exif data from images found in `_lf_images`. It adds to each
entry in `_ls_images` a field named `exif` which is an object containing all
exif data.
