_ = require 'lodash'
Promise = require 'bluebird'
request = Promise.promisifyAll require('request')
http = require 'http'
https = require 'https'
url = require 'url'
gm = require 'gm'
winston = require 'winston'
debug = require('debug')('☸ plugin.exif')

hasher = require('littlefork').hasher

# Setup logging.
winston.remove winston.transports.Console
winston.add winston.transports.Console, timestamp: true, colorize: true

imageExifHasher = hasher.fieldHasher '_lf_exif_hash', ['exif.Format',
                                                       'exif.format',
                                                       'exif.size.width',
                                                       'exif.size.height',
                                                       'exif.Geometry']

fetchExif = (uri) ->
  protocol = url.parse(uri).protocol
  get = switch protocol
    when 'http:' then http.get
    when 'https:' then https.get
    else throw new Error("Can't handle #{protocol} as URI endpoint.")

  new Promise (resolve, reject) ->
    get(uri, (res) ->
      gm(res).identify((err, data) ->
        if err then reject(err)
        else resolve(data))
    ).on 'error', (e) -> reject e

# MongoDB doesn't like dots in keys.
normalizeDotsInKeys = (exif) ->
  _.reduce exif, (memo, v, k) ->
    key = k.split('.').join('_')
    memo[key] = v
    memo
  , {}

module.exports = (val) ->
  data = val.data
  envelope = _.extend val, data: []
  omitFields = process.env.EXIF_OMIT_FIELDS?.split(',') or []
  pickFields = process.env.EXIF_PICK_FIELDS?.split(',') or []

  omitExif = if _.size(omitFields) > 0 then (exif) -> _.omit exif, omitFields
  else _.identity
  pickExif = if _.size(pickFields) > 0 then (exif) -> _.pick exif, pickFields
  else _.identity

  exifTransformer = _.flow omitExif, pickExif, normalizeDotsInKeys
  unitTransformer = _.flow imageExifHasher

  Promise.reduce data, (memo, unit) ->
    Promise.map unit._lf_images or [], (img) ->
      fetchExif img.href
      .catch (e) ->
        debug e.msg
        debug e.stack
      .then (data) -> _.extend img, exif: exifTransformer(data)
    .then (images) ->
      memo.data.push unitTransformer(_.extend unit, _lf_images: images)
      memo
  , envelope

module.exports.argv =
  'exif.omit_fields':
    nargs: 1
    type: 'string'
    desc: 'Omit those fields from the exif output.'
  'exif.pick_fields':
    nargs: 1
    type: 'string'
    desc: 'Pick those fields from the exif output.'
